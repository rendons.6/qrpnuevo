import { Component } from '@angular/core';
import { NavController, NavParams, AlertController, ActionSheetController, ModalController } from 'ionic-angular';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Cliente } from '../../app/model/cliente.model';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  clienteCollection: AngularFirestoreCollection<Cliente>;
  clientes: Cliente[];

  constructor(

    public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public angularFirestore: AngularFirestore,
    public alertCtrl: AlertController,
    public actionSheetCtrl: ActionSheetController,


  ) {

  }

  ionViewDidLoad() {

    this.clienteCollection = this.angularFirestore.collection('clientes');

    this.clienteCollection.snapshotChanges().subscribe(customerList => {
      this.clientes = customerList.map(item => {
        return {
          idDoc: item.payload.doc.id,
          nombre: item.payload.doc.data().nombre,
          apellido: item.payload.doc.data().apellido,
          edad: item.payload.doc.data().edad,

        };
      });

    });
  }

  openItem(cliente: Cliente) {
    const actionSheet = this.actionSheetCtrl.create({
      title: 'Opciones del cliente',
      buttons: [
      
        {
          text: 'Eliminar',
          role: 'destructive',
          icon: 'md-trash',
          handler: () => {
            this.deleteItem ( cliente );
            console.log('Destructive clicked');
          }
        },
        {
          text: 'Información del cliente',
          icon: 'md-information-circle',
          handler: () => {
            const addModal = this.modalCtrl.create('DetalleClientePage', { 'cliente': cliente });
            addModal.present();
            console.log('Archive clicked');

          }
        }, {
          text: 'Editar',
          role: 'md-create',
          icon: 'md-person-add',
          handler: () => {
            this.editItem(cliente);
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          icon: 'md-close',
          handler: () => {
            console.log('Cancel clicked');

          }
        }
      ]

    });
    actionSheet.present();
  }
  addItem() {
    const addModal = this.modalCtrl.create('CrearClientePage');
    addModal.onDidDismiss(cliente => {
      if (cliente) {
        this.angularFirestore.collection('clientes').add({
          nombre: cliente.nombre,
          apellido: cliente.apellido,
          edad: cliente.edad,
        }).then(newItem => {
          alert('El cliente ' + cliente.nombre + ' se ha guardado exitosamente');
        }).catch(error => {
          this.registroErroneo(error);
          console.log(error);
        });
      }
    });
    addModal.present();
  }

  registroErroneo(error) {
    const alert = this.alertCtrl.create({
      title: 'Que Mal :(',
      subTitle: 'Ha Ocurrido un error en el registro el cliente' + error,
      buttons: ['Cerrar']
    });
    alert.present();
  }
  editItem(clienteEdit: Cliente) {
    const addModal = this.modalCtrl.create('CrearClientePage', { 'clienteEdit': clienteEdit });
    addModal.onDidDismiss(item => {
      if (item) {
        const batch = this.angularFirestore.firestore.batch();
        const customer = this.angularFirestore.doc(`clientes/${clienteEdit.idDoc}`).ref;
        batch.update(customer, {
          nombre: item.nombre,
          apellido: item.apellido,
          edad: item.edad,
        });
        batch.commit().then (()=>{
          alert('El cliente ' + item.nombre + ' ' + item.apellido + ' ha sido editado exitosamente');
        }).catch(error =>{
          this.registroErroneo(error);
        });
      }
    });
    addModal.present();
  }

  deleteItem(clienteEliminar: Cliente) {
        const batch = this.angularFirestore.firestore.batch();
        const clienteDelete = this.angularFirestore.doc(`clientes/${clienteEliminar.idDoc}`).ref;
        batch.delete(clienteDelete);
        batch.commit ().then (() => {
          alert('El cliente ha sido eliminado correctamente');
        }).catch(error => {
          this.registroErroneo(error);
        });
  }
}