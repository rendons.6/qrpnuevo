import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Cliente} from '../../app/model/cliente.model'; 
/**
 * Generated class for the CrearClientePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-crear-cliente',
  templateUrl: 'crear-cliente.html',
})
export class CrearClientePage {

  form: FormGroup;
  isReadyToSave: boolean;
  clienteEdit: Cliente;

  constructor(
    public navParams: NavParams,
    public viewCtrl: ViewController,
    formBuilder: FormBuilder) {

      this.form = formBuilder.group({
        nombre: ['', Validators.required],
        apellido: ['', Validators.required],
        edad: [''],
      });

      if (this.navParams.get('clienteEdit')) {
        this.clienteEdit = this.navParams.get ('clienteEdit');
        this.form.patchValue(this.clienteEdit);
      }

      //whatch the form for changes, and
      this.form.valueChanges.subscribe((v)=>{
        this.isReadyToSave = this.form.valid;
      });
  }
    cancel() {
      this.viewCtrl.dismiss();
    }

    done() {
      if (!this.form.valid) { return; }
      this.viewCtrl.dismiss(this.form.value);
    }

  

}
